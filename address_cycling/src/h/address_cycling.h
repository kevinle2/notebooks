#define addrLimit 15
#define pinMax 4

const int addrPins[] = {46,3,8,18};
const int segmentPins[] = {17,16,15,7,6,5,4};
const int ledPins[] = {17,16,15,7,6,5,4,42,2,1};

int address;
int plants;

const byte hexMap[16][7]={
// a,b,c,d,e,f,g
  {1,1,1,1,1,1,0}, // 0
  {0,1,1,0,0,0,0}, // 1
  {1,1,0,1,1,0,1}, // 2
  {1,1,1,1,0,0,1}, // 3
  {0,1,1,0,0,1,1}, // 4
  {1,0,1,1,0,1,1}, // 5
  {1,0,1,1,1,1,1}, // 6
  {1,1,1,0,0,0,0}, // 7
  {1,1,1,1,1,1,1}, // 8
  {1,1,1,1,0,1,1}, // 9
  {1,1,1,0,1,1,1}, // A
  {0,0,1,1,1,1,1}, // b
  {1,0,0,1,1,1,0}, // C
  {0,1,1,1,1,0,1}, // d
  {1,0,0,1,1,1,1}, // E
  {1,0,0,0,1,1,1}};// F

void resetHex(){
  for(byte i = 0 ; i < 7 ; i++){digitalWrite(segmentPins[i], LOW);}
}

void setHex(){
  resetHex();
  for(byte i = 0 ; i < 7 ; i++){
    (hexMap[address][i]==1) ? digitalWrite(segmentPins[i], HIGH) : digitalWrite(segmentPins[i], LOW);
    }
}

// output current address
void setAddr(){ 
  if(address > addrLimit){
    address = 0;
  }
  
}
void setAddrPins(int addr){
  for (byte i=0; i<pinMax; i++) {
    int state = bitRead(addr, i);
    digitalWrite(addrPins[i], state);
  }
}

void addrCycle(){////
  address = (address >= plants) ? 0 : address;
  setAddr();
  // setHex();
  address++;
}
