#include <Arduino.h>
#include <h/prototypes.h>
#include <h/address_cycling.h>
#include <driver/gpio.h>
#include <array> // for array, at()
#include <Wire.h>

#define thresh 580 

#define interval 10 // (microseconds)
#define dataSize 10
#define MAXADDR 1

#define write_pin 9
#define open_pin 10 
#define mosipin 14
#define misopin 13
#define clkpin 12

#define offset1 3
#define offset2 (offset1 + 1000)//long delay to allow sensor to power on
#define offset3 (offset2 + 18 + 20)
#define offset4 (offset3 + 1000)//long delay to allow sensor to power on
#define offset5 (offset4 + 16 + 2)
#define offset6 (offset5 + 10)
#define offset7 (offset6 + 20)


int cycle;
int clk;
int din;
int on;

std::array<int,dataSize> channel1;
std::array<int,dataSize>  channel2;

int writesig;
int opensig;
int topval;
int drainval;

unsigned long previousMicros = 0;  // will store last time LED was updated

// the setup function runs once when you press reset or power the board
void setup() {
  Serial.begin(9600); 
  // while(!Serial);  
  setPins();    
  pinMode(4, OUTPUT);
  analogWrite(4,220);

  plants = 15;
  address = 0;
  cycle = 0;
  clk = 0;
  din = 0;
  writesig = 0;
  opensig = 0;
  topval = 0;
  drainval = 0;
  on = 1;
  for(int i = 0; i < 1; i++){
    pinMode(ledPins[i],OUTPUT); 
  }

}

// the loop function runs over and over again forever
void loop() {
  unsigned long currentMicros = micros();
  if (currentMicros - previousMicros >= interval) {
    previousMicros = currentMicros;
    clk = ~clk;
    digitalWrite(clkpin, clk);
    topval = 0;
    for (int i = 0; i < 10; i++){
      topval = topval + channel1[i] * pow(2, i);//find binary number
    }
    for (int i = 0; i < 10; i++){
      drainval = drainval + channel2[i] * pow(2, i);//find binary number
    }
    
    Serial.println(drainval);

    if(clk == 0){
      cycle++;
    }
    //clk and cycle are the new values we just started at this halfperiod
    //do stuff
    if(clk==0 && cycle==0){
      //keep track of address but only selectively set the bits
      //assume address is correct, will not change until end of total operation
      //assume write was just set to 0
      din = 0;
      digitalWrite(mosipin, din);
    }
    if(clk==0 && cycle==offset1){
      setAddrPins(address);//address is sent to slaves
    }
    if(clk==0 && cycle==(offset2)){
      din = 1;
      digitalWrite(mosipin, din);//start bit
    }
    if(clk==0 && cycle==(offset2 + 1)){
      din = 1;
      digitalWrite(mosipin, din);//single ended
    }
    if(clk==0 && cycle==(offset2 + 2)){
      din = 0;
      digitalWrite(mosipin, din);//d2 (don't care)
    }
    if(clk==0 && cycle==(offset2 + 3)){
      din = 0;
      digitalWrite(mosipin, din);//d1
    }
    if(clk==0 && cycle==(offset2 + 4)){
      din = 0;
      digitalWrite(mosipin, din);//d0
    }
    if(clk!=0 && cycle==(offset2 + 7)){
      channel1[9] = digitalRead(misopin);//most sig bit of ouptut
    }
    if(clk!=0 && cycle==(offset2 + 8)){
      channel1[8] = digitalRead(misopin);
    }
    if(clk!=0 && cycle==(offset2 + 9)){
      channel1[7] = digitalRead(misopin);
    }
    if(clk!=0 && cycle==(offset2 + 10)){
      channel1[6] = digitalRead(misopin);
    }
    if(clk!=0 && cycle==(offset2 + 11)){
      channel1[5] = digitalRead(misopin);
    }
    if(clk!=0 && cycle==(offset2 + 12)){
      channel1[4] = digitalRead(misopin);
    }
    if(clk!=0 && cycle==(offset2 + 13)){
      channel1[3] = digitalRead(misopin);
    }
    if(clk!=0 && cycle==(offset2 + 14)){
      channel1[2] = digitalRead(misopin);
    }
    if(clk!=0 && cycle==(offset2 + 15)){
      channel1[1] = digitalRead(misopin);
    }
    if(clk!=0 && cycle==(offset2 + 16)){
      channel1[0] = digitalRead(misopin);//lsb of adc
    }
    if(clk==0 && cycle==(offset2 + 18)){
      setAddrPins(15);//flick address off ie pull chip select high
    }
    if(clk==0 && cycle==(offset3)){
      setAddrPins(address);//pull chip select back down
    }
    if(clk==0 && cycle==(offset4)){
      din = 1;
      digitalWrite(mosipin, din);//start bit
    }
    if(clk==0 && cycle==(offset4 + 1)){
      din = 1;
      digitalWrite(mosipin, din);//single ended
    }
    if(clk==0 && cycle==(offset4 + 2)){
      din = 0;
      digitalWrite(mosipin, din);//d2 (don't care)
    }
    if(clk==0 && cycle==(offset4 + 3)){
      din = 0;
      digitalWrite(mosipin, din);//d1
    }
    if(clk==0 && cycle==(offset4 + 4)){
      din = 1;
      digitalWrite(mosipin, din);//d0 select second channel
    }
    if(clk!=0 && cycle==(offset4 + 7)){
      channel2[9] = digitalRead(misopin);//most sig bit of ouptut
    }
    if(clk!=0 && cycle==(offset4 + 8)){
      channel2[8] = digitalRead(misopin);
    }
    if(clk!=0 && cycle==(offset4 + 9)){
      channel2[7] = digitalRead(misopin);
    }
    if(clk!=0 && cycle==(offset4 + 10)){
      channel2[6] = digitalRead(misopin);
    }
    if(clk!=0 && cycle==(offset4 + 11)){
      channel2[5] = digitalRead(misopin);
    }
    if(clk!=0 && cycle==(offset4 + 12)){
      channel2[4] = digitalRead(misopin);
    }
    if(clk!=0 && cycle==(offset4 + 13)){
      channel2[3] = digitalRead(misopin);
    }
    if(clk!=0 && cycle==(offset4 + 14)){
      channel2[2] = digitalRead(misopin);
    }
    if(clk!=0 && cycle==(offset4 + 15)){
      channel2[1] = digitalRead(misopin);
    }
    if(clk!=0 && cycle==(offset4 + 16)){
      channel2[0] = digitalRead(misopin);//lsb of adc second channel
    }
    if(clk==0 && cycle==(offset5)){
      opensig = makeDecision();//MAKE DECISION AND OUTPUT IT BEFORE RAISING WRITE SIGNAL
      digitalWrite(open_pin, opensig);//opensig
      // if(address == 1){ 
      //   if (on == 0){
      //     on = 1;
      //   }
      //   else{
      //     on = 0;
      //   }
      // }
      // else{}
    }
    if(clk==0 && cycle==(offset6)){
      writesig = 1;//change to write operation
      din = 0;
      digitalWrite(mosipin, din);
      digitalWrite(write_pin, writesig);
    }
    if(clk!=0 && cycle==(offset7)){//1
      writesig = 0;
      digitalWrite(write_pin, writesig);
      cycle = -1;
      if (address == MAXADDR){
        address = 0;
      }
      else{
        address++;
      }
      setAddrPins(15);//flick address off
    }
  }  
}
int makeDecision(){//change this later
  return (topval < thresh)? 1 : 0;
}

void setPins(){
  pinMode(write_pin,OUTPUT);
  pinMode(open_pin,OUTPUT);
  pinMode(clkpin,OUTPUT);
  pinMode(misopin,INPUT);
  pinMode(mosipin,OUTPUT);
  digitalWrite(write_pin, LOW);
  digitalWrite(open_pin, LOW);
  digitalWrite(clkpin, LOW);
  digitalWrite(mosipin, LOW);

  for(int i = 0 ; i < 10 ; i ++){
    if(i < pinMax){
      pinMode(addrPins[i], OUTPUT);
      digitalWrite(addrPins[i], LOW);
    }
  }
}







