# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.


## Data Flow

Frontend -> ESP32
Input from User: 
- Plant Name: string
- Minimum Water Level: string
- Watering Hours: string
- Maximum Water Usage: string

Output:
- Plant Name: string
- Minimum Water Level: string
- Watering Hours: string
- Maximum Water Usage: string

ESP32 -> Frontend
Input from Sensors:
- Current Moisture Level: tbd
- Current Water Usage: tbd

Output:
- Current Moisture Level: string
- Current Water Usage: string

# How things work
All the variables uptop uses useState React hook which initiates the variable and a function that can be called to set the variable data. This is how we store data globally.

Modal is for user to input the necessary parameters to send to the ESP32. 

Connecting to ESP32 via Websocket for data transmission
Documentation: https://www.npmjs.com/package/react-use-websocket
- websocket takes in the ESP32 IP address and gives us:
    - sendMessage: allows us to send data to the ESP32 from the frontend
    - lastMessage: data ESP32 sends frontend
    - readyState: status of Websocket connections
- UseEffect React hook to constantly fetch and update the data everytime there is a change in the lastMessage variable. This allows us to do live updating.

Everything else is self explanitory, it is how the webpage is laid out, nothing super fancy. 










