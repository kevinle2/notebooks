import {gql} from '@apollo/client';

export const CREATE_PLANT_MUTATION = gql`
mutation createPlant($plantName: String! $maxWaterUsage: String! $minMoistureLevel: String! $hours: String!) {
    createPlant(plantName: $plantName maxWaterUsage: $maxWaterUsage minMoistureLevel: $minMoistureLevel hours: $hours) {
        plantName
        maxWaterUsage
        minMoistureLevel
        hours
    }
}
`