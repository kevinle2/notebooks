import {gql} from '@apollo/client';

export const PLANTS_QUERY = gql`
query {
    plants {
        id
        plantName
        maxWaterUsage
        minMoistureLevel
        hours
    }
}
`


