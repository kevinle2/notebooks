import React, { useState, useEffect } from 'react';
import { useQuery, gql } from '@apollo/client';
import { PLANTS_QUERY } from '../GraphQL/Queries';

function GetPlants() {
    const [plants, setPlants] = useState([])
    const {error, loading, data} = useQuery(PLANTS_QUERY)


    useEffect(() => {
        if (data) {
            setPlants(data.plants)
        }
    }, [data])

    return (
        <div>
            {plants.map((val) => {
                return <h1>{val.plantName}</h1>
            })}
        </div>
    )
}

export default GetPlants;