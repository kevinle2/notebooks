import React, { useState } from "react";
import { CREATE_PLANT_MUTATION } from "../GraphQL/Mutations";
import { useMutation } from "@apollo/client";

function Form() {
  const [plantName, setPlantName] = useState("");
  const [maxWaterUsage, setMaxWaterUsage] = useState("");
  const [minMoistureLevel, setMinMoistureLevel] = useState("");
  const [hours, setHours] = useState("");

  const [createPlant, { error }] = useMutation(CREATE_PLANT_MUTATION);

  const addPlant = () => {
    createPlant({
      variables: {
        plantName: plantName,
        maxWaterUsage: maxWaterUsage,
        minMoistureLevel: minMoistureLevel,
        hours: hours,
      },
    });

    if (error) {
      console.log(error);
    }
  };
  return (
    <div>
      <input
        type="text"
        placeholder="Plant Name"
        onChange={(e) => {
          setPlantName(e.target.value);
        }}
      />
      <input
        type="text"
        placeholder="Max Water Usage"
        onChange={(e) => {
          setMaxWaterUsage(e.target.value);
        }}
      />
      <input
        type="text"
        placeholder="Min Moisture Level"
        onChange={(e) => {
          setMinMoistureLevel(e.target.value);
        }}
      />
      <input
        type="text"
        placeholder="Hours"
        onChange={(e) => {
          setHours(e.target.value);
        }}
      />
      <button onClick={addPlant}> Create Plant</button>
    </div>
  );
}

export default Form;