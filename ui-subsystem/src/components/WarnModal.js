import React from 'react';
import './WarnModal.css'

const WarnModal = ({ isOpen, warnedPlants, closeModal }) => {
  if (!isOpen) {
    return null;
  }

  return (
    <div className="warn-modal-backdrop">
      <div className="warn-modal-content">
        <h2>Warning</h2>
        <div className="warn-modal-body">
          {warnedPlants.map((plant, index) => (
            <p key={index}>Plant Address: {plant.address}, Health: {plant.health}</p>
          ))}
        </div>
        <button onClick={closeModal}>Close</button>
      </div>
    </div>
  );
};

export default WarnModal;
