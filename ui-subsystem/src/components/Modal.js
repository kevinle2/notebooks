// Modal.js
import React, { useState } from 'react';
import Modal from 'react-modal';

Modal.setAppElement('#root'); // Set the root element for accessibility

const MyModal = ({ isOpen, closeModal, sendDataToParent }) => {
  const [formData, setFormData] = useState({});

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    sendDataToParent(formData); // closeModal will be called inside receiveData now
};


  return (
    <Modal isOpen={isOpen} onRequestClose={closeModal} >
      <h2>Fill in Information</h2>
      
      <form onSubmit={handleSubmit}>
        <label htmlFor="plantName">Plant Name:</label>
        <input
          type="text"
          id="plantName"
          name="plantName"
          value={formData['plantName'] || ''}
          onChange={handleInputChange}
        />

        {/* <label htmlFor="maxWaterUsage">Max Water Usage:</label>
        <input
          type="text"
          id="maxWaterUsage"
          name="maxWaterUsage"
          value={formData['maxWaterUsage'] || ''}
          onChange={handleInputChange}
        /> */}

        <label htmlFor="minMoistureLevel">Min Moisture Level:</label>
        <input
          type="text"
          id="minMoistureLevel"
          name="minMoistureLevel"
          value={formData['minMoistureLevel'] || ''}
          onChange={handleInputChange}
        />

        <label htmlFor="wateringHours">Watering Hours:</label>
        <input
          type="text"
          id="wateringHours"
          name="wateringHours"
          value={formData['wateringHours'] || ''}
          onChange={handleInputChange}
        />

        <label htmlFor="wateringDays">Watering Days:</label>
        <input
          type="text"
          id="wateringDays"
          name="wateringDays"
          value={formData['wateringDays'] || ''}
          onChange={handleInputChange}
        />

        <button type="submit">Submit</button>
      </form>
    </Modal>
  );
};

export default MyModal;
