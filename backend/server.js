const express = require('express')
const {graphqlHTTP} = require('express-graphql')
const cors = require("cors");

const PORT = 5000;
const app = express()
app.use(cors());
app.use(express.json());
const schema = require('./Schemas/index.js');




app.use('/graphql', graphqlHTTP({
    schema: schema,
    graphiql: true
}))
app.listen(PORT, () => console.log('Server Running'))
