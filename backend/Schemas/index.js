const {
    GraphQLSchema,
    GraphQLObjectType,
    GraphQLString,
    GraphQLList,
    GraphQLInt,
    GraphQLNonNull
} = require('graphql')

const PLANT_DATA = require('../Data/PLANT_DATA.json')
const PlantType = require('./TypeDefs/PlantType')

const RootQueryType = new GraphQLObjectType({
    name: 'Query',
    description: 'Root Query',
    fields: () => ({
        plants: {
            type: new GraphQLList(PlantType),
            description: 'A list of all plants',
            resolve: () => PLANT_DATA
        },
    })
})

const Mutation = new GraphQLObjectType({
    name: "Mutation",
    fields: {
        createPlant: {
            type: PlantType,
            args: {
                plantName: { type: GraphQLNonNull(GraphQLString) },
                maxWaterUsage: { type: GraphQLNonNull(GraphQLString) },     // might need to change to int later
                minMoistureLevel: { type: GraphQLNonNull(GraphQLString) },
                hours: { type: GraphQLNonNull(GraphQLString) },
            },
            resolve(parent, args) {
                PLANT_DATA.push({id: PLANT_DATA.length + 1, plantName: args.plantName, maxWaterUsage: args.maxWaterUsage, minMoistureLevel: args.minMoistureLevel, hours: args.hours})
                return args
            }
        }
    }
})

module.exports = new GraphQLSchema({
    query: RootQueryType,
    mutation: Mutation
})