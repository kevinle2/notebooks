const graphql = require('graphql');
const {
    GraphQLObjectType,
    GraphQLString,
    GraphQLInt,
    GraphQLNonNull
} = require('graphql');

const PlantType = new GraphQLObjectType({
    name: 'Plant',
    description: 'This represents a plant',
    fields: () => ({
        // id: { type: GraphQLNonNull(GraphQLInt) },
        plantName: { type: GraphQLNonNull(GraphQLString) },
        maxWaterUsage: { type: GraphQLNonNull(GraphQLString) },     // might need to change to int later
        minMoistureLevel: { type: GraphQLNonNull(GraphQLString) },
        hours: { type: GraphQLNonNull(GraphQLString) },
    })
})

module.exports = PlantType;