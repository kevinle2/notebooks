/*
  WiFiAccessPoint.ino creates a WiFi access point and provides a web server on it.

  Steps:
  1. Connect to the access point "yourAp"
  2. Point your web browser to http://192.168.4.1/H to turn the LED on or http://192.168.4.1/L to turn it off
     OR
     Run raw TCP "GET /H" and "GET /L" on PuTTY terminal with 192.168.4.1 as IP address and 80 as port

  Created for arduino-esp32 on 04 July, 2018
  by Elochukwu Ifediora (fedy0)

  test network: 
  ssid: Caffebene-Custmer
  pw: Benebeans
*/

#include <Arduino.h>
#include <h/prototypes.h>
#include <h/plantWIFI.h>
#include <h/helpers.h>
#include <WiFi.h>
#include <WiFiClient.h>
#include <WiFiAP.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>
// #include <EEPROM.h>

#define wifi_status 45
#define wifi_success 48
#define red 21

AsyncWebServer server(80);

void setup() {
  Serial.begin(115200);
  while(!Serial);
  initIO();
  initWIFI();
  start();
  char * WIFI;
  char * PW;

  // init storage system for wifi credentials

  // initialize wifi components
  // wifiConfig(&WIFI,&PW);

  while(!connectWiFi(&WIFI,&PW));
  // save wifi creds if not done so already
}

void loop() {
}



void initWIFI(){
  WiFi.mode(WIFI_AP_STA);
  WiFi.disconnect();
}

// wifi function to connect to a local WIFI network
bool connectWiFi(char ** WIFI, char ** PW) {
  int ct = 0;
  WiFi.begin("Twer at Third");
  Serial.print("\nConnecting to WiFi ..");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    setLED(wifi_status,LOW);
    ct++;
    if(ct > 25){
      setLED(wifi_status,HIGH);
      return false;
    }
    setLED(wifi_status,HIGH);
  }
  setLED(wifi_success,LOW);
  Serial.println();
  Serial.println(WiFi.localIP());
  return true;
}

// configure access point to include user input
void wifiConfig(char ** WIFI, char ** PW){
  Serial.println();
  Serial.println("Configuring access point...");

  if (!WiFi.softAP(soft_ssid, soft_password)) {
    log_e("Soft AP creation failed.");
    while(1);
  }
  server.begin();
  Serial.println("Server started");

  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(200, "text/html", index_html);
  });

  server.on("/get", HTTP_GET, [] (AsyncWebServerRequest *request) {
    const char* WIFI_var;
    const char* PW_var;
    // GET input1 value on <ESP_IP>/get?input1=<inputMessage>
    if (request->hasParam("ssid") && request->hasParam("password")) {
      WIFI_var = request->getParam("ssid")->value().c_str();
      PW_var = request->getParam("password")->value().c_str();
      // writeString(WIFI_var,0);
      // writeString(PW_var,30);
    }
    else {
      WIFI_var = "error";
      PW_var = "error";
    }
    Serial.printf(WIFI_var);
    request->send(200, "text/html", "HTTP GET request sent to your ESP on input field <br><a href=\"/\">Return to Home Page</a>");
  });
  server.begin();
  // *WIFI = strdup(*readStringFromFlash(0).c_str());
  // Serial.print(readStringFromFlash(0));
}

// void writeString(const char* toStore, int startAddr) {
//   int i = 0;
//   for (; i < LENGTH(toStore); i++) {
//     EEPROM.write(startAddr + i, toStore[i]);
//   }
//   EEPROM.write(startAddr + i, '\0');
//   EEPROM.commit();
// }

// String readStringFromFlash(int startAddr) {
//   char in[30];
//   char curIn;
//   int i = 0;
//   curIn = EEPROM.read(startAddr);
//   for (; i < 30; i++) {
//     curIn = EEPROM.read(startAddr + i);
//     in[i] = curIn;
//   }
//   return String(in);
// }



// void initStorage(){
//   //eeprom


// }
