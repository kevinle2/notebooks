#define wifi_status 45
#define wifi_success 48
#define red 21
// start button function


void initIO(){
  pinMode(wifi_status, OUTPUT);
  pinMode(wifi_success, OUTPUT);
  pinMode(red, OUTPUT);
  LEDReset();
}

// set start state led to red
void start(){
  setLED(red,LOW);
  LEDReset();
}

void setLED(int pin, int stat){
  digitalWrite(pin,stat);
  delay(250);
}

void LEDReset(){
  setLED(wifi_status,HIGH);
  setLED(wifi_success,HIGH);
  setLED(red,HIGH);
}
