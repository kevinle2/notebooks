#define LENGTH(x) (strlen(x) + 1)
#define schedule_speed 500



// input html
const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html><head>
  <title>ESP Input Form</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  </head><body>
  <h1>Enter WIFI Credentials</h1>
  <form action="/get">
    ssid: <input type="text" name="ssid">
    password: <input type="text" name="password">
    <input type="submit" value="Submit">
  </form><br>
</body></html>)rawliteral";

// Set these to your desired credentials.
const char *soft_ssid = "PLANT_SYS";
const char *soft_password = "pass1234";

const byte hex_map[16][7]={
                  {1,1,1,1,1,1,0},
                  {0,1,1,0,0,0,0}, 
                  {1,1,0,1,1,0,1}, 
                  {1,1,1,1,0,0,1},
                  {0,1,1,0,0,1,1},
                  {1,0,1,1,0,1,1},
                  {1,0,1,1,1,1,1},
                  {1,1,1,0,0,0,0},
                  {1,1,1,1,1,1,1},
                  {1,1,1,1,0,1,1},
                  {1,1,1,0,1,1,1},
                  {0,0,1,1,1,1,1},
                  {1,0,0,1,1,1,1},
                  {0,1,1,1,1,0,1},
                  {1,0,0,1,1,1,1},
                  {1,0,0,0,1,1,1}};
