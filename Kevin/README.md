# Kevin's Worklog
* 09/25/23 - Project Planning with group
* 10/02/23 - Design Document review with Professor Mironenko
* 10/09/23 - Frontend design/programming
* 10/16/23 - WiFi communication design/Slave Testing
* 10/23/23 - Data transmission
* 10/30/23 - Data Transmission continued
* 11/06/23 - Integrating all Subsystems
* 11/20/23 - Fall Break
* 11/27/23 - Final Integeration and Testing/Failure

# 09/25/23 - Project Planning with group
TA's suggested that we migrate our initial project proposal of building a plant gardening system that moderates the amount of sunlight a plant can take to a water based plant irrigation system. We brainstormed the specific components we would need for the irrigation system. This included defining the slave and master design elements as well as the initial pcb design. We sat down and John drew out a  large schematic diagram that we all walked through to get the proper logic of the communication protocol. Once we roughly figured out the design elements, we made a list of parts we need and did research on different sources and places where we could buy them from. We tried to optimize the cost so we looked for the cheapest possible parts that we could buy. 

# 10/02/23 - Design Document review with Professor Mironenko
Have a very insightful conversation and discussion with Professor Mironenko on our project scope and certain elements of our project. One specific component discussed was our Power Subsystem. Initially we were going to use a store bought power block component that gives us 3.3v, but after our discussion with the professor, she said that we should be creating our own subsystem instead of using already made products. We then modified our design to have a step down from a 5v to a 3.3v using an AC/DC Converter. Asides from that everything checked out, just some small feedback on our formatting of the design doc for having smaller sentences for requirements and how we should structure our RV table should be written.


# 10/09/23 - Frontend design
While waiting for parts to arrive, I started working on the UI-Subsystem which involved a webpage which the user uses to send information to the microcontroller to control the plant's watering settings. I used Figma and made a visual design of what the webpage should look like before coding it. The necessary parameters to the user should control is the plant name, the maximum water usage, the minimum moisture level, and the scheduling hours. The maximum water usage and scheduling hours is for ethics and safety to abide to state watering laws. The minimum moisture level is the minimum level the plant needs to stay at to be healthy. These are the information needed to be sent to the microcontroller. Also planned for having a configuration save button to save plant settings to be reused in the future. 

Also added a current health section where the user can see the current health of each of their plants. The data of the plant's address and plant's current health is sent from the microcontroller to be displayed on the webpage. 


I coded the webpage using React.js and CSS for syling to make the page pretty and legible. No issues here. Issues came to saving the plant configuration. Initially I made a backend with GraphQL, which is a query language to store and create new configurations. I had most of this done but it became a huge headache and I needed to work on other components of the project. I talked with the team and we decided to scrap this nice feature as it is just a bonus feature and we can add it later if there is time. At this point, the UI subsystem is halfway done with the frontend skeleton being done with only a few minor visual bugs that I plan to fix later. The bugs included a skewed display of plants added (probably due to an indexing error) and bonus syling needed to make the data shown look nicer. 

There is no communication with the microcontroller yet and is something that needs to be worked on next.

# 10/16/23 - Wifi communication design
After finishing most of the frontend part of the UI-Subsystem, I now needed to enable communication between the frontend and the ESP32 microcontroller that we specifically chose because it enables Wifi for communication. This was done using a built in WiFi library called WiFi.h, where it allows the ESP32 to act as a station and/or an access point where it can send and recieve information. Regarding WiFi security issues, we thought to save the WiFi credentials to the ESP32 as it is much safer to being hacked than saving it on the users local device, so the WiFi is enabled first enabled as station to let devices connect to it then it switches to an access point to save the WiFi credentials for security. I never worked with WiFi and microcontroller code before so this was all new to me and I am going of research and documentation. 

Carlos and I both worked on this part parallely while waiting for our parts and PCB, so we were able to cross check with each other if we ran into issues. One issue that we ran into was connecting to the University's WiFi because it needed specific credentials. We were able to overcome this using the University's guest network and registering our device so that it is recognized everytime. Carlos came up with that solution. 

This week I also spent time with John working on validating the slave hardware design. John built the entire slave as that is part of the subsystem that he was assigned to and I was there to help him debug what he had built. After spending a while checking wires and making sure everything was in place, we finally figured out what was wrong with the board. Actually nothing was wrong with the board, it was just that when we were testing with the sensors, John was holding the two sensors simultaneously, which made a current flow through him giving us an unexpected value. He then realized this and told me to hold one sensor while he held the other, which gave us the proper value ensuring the slave design works properly. 


# 10/23/23 - Data Transmission continued
I researched on how messages are actually sent between a microcontroller and a React webapp. Initially I used HTTP requests, first trying to get hardcoded data from the ESP32 to show up on the frontend, which was easier to display than vise-versa. I knew that the connection works properly through thorough testing of the WiFi using LED's and print statements, but nothing was being displayed when trying to send data from the ESP32 over to the frontend.For the week I was really stuck here for a while as I was confident HTTP requests was the proper way for message communication as I've done something similar before. Started researching other methods of communication

# 10/30/23 - Integrating all Subsystems
 I did some more research and looked into Websockets, which was something new to me. There was already a Websocket library with an example almost identical to the problem I had at the time, so I tried that. The messages now displayed properly. I learned that the issue with HTTP requests is that while WebSockets enables realtime bidirectional communication between the server and the client, while the issue with the HTTP requests is a request-response model so it only responds to the clients, but we needed two way communication. I tested sending the user inputted data from the frontend to the ESP32 and I can now see that same data being printed in the Ardiuno terminal. Now the UI-Subsystem is completed and working with hardcoded data, all that is left is to use real data once we combine all the subsystems.

# 11/06/23 - Integrating all Subsystems
Met up with group to integrate all our subsystems. Spent most of my time helping create the physical design of our system. Used dremel to drill slave holes and super glue to connect things together. Spent a lot of time preping wires and making sure they are prepared and water proofing them.

# 11/20/23 - Fall Break
Did not really do much

# 11/27/23 - Final Integration and Testing/Failure
Doing the finishing integration and testing of our system. It failed unexpectedly. Spent the entire time until demo trying to debug. We fixed some parts of it due to faulty wiring when connecting it after waterproofing but some other parts such as multi slave did not work. 